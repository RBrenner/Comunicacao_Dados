#include <SoftwareSerial.h>
#include <stdlib.h>
#include<cstdlib>
SoftwareSerial rs485(14, 12, false, 256);


const uint8_t BUFFER_SIZE = 20;
uint8_t size = 0;
char buffer[BUFFER_SIZE];
uint8_t le_entrada1[6]={0,1,8,11,0,20};
uint8_t liga_1[6]={0,1,8,13,17,39};
uint8_t desliga_1[6]={0,1,8,13,15,37};
uint8_t le_entrada2[6]={0,2,8,11,0,21};
uint8_t liga_2[6]={0,2,8,13,17,40};
uint8_t desliga_2[6]={0,2,8,13,15,38};
int i=0;
int j=0;
byte x=0;
byte z[6]={1,2,3,4,5,6};
byte sum;
int ctrl=0;

int checksum()
{
  sum = ((z[0] + z[1] + z[2] + z[3] +z[4] -z[5]));
  if (sum==0){
  return 1; 
  }
  else{
  return 0;
  }
}
 

void entrada_1() {
           digitalWrite(D7, HIGH);
           for (int i=0; i <=5 ;i++)
           {
            // Serial.println(le_entrada1[i]); //mostra no monitor serial o dado recebido
             rs485.write(le_entrada1[i]);
           }
            ctrl=1;
            digitalWrite(D7, LOW);
            x = rs485.read(); //leitura para limpar o buffer
            delay(30);
}

            
void liga_led1() {
           digitalWrite(D7, HIGH);
           delay(10);
           for (int i=0; i <=5 ;i++)
           {
           // Serial.println(liga_1[i]); //mostra no monitor serial o dado recebido
            rs485.write(liga_1[i]);
           }
           ctrl=2;
            digitalWrite(D7, LOW);
            x = rs485.read(); //leitura para limpar o buffer
            delay(30);
           }
           
void desliga_led1() {
           digitalWrite(D7, HIGH);
           delay(10);
           for (int i=0; i <=5 ;i++)
           {
            // Serial.println(desliga_1[i]); //mostra no monitor se/rial o dado recebido
             rs485.write(desliga_1[i]);    
           }
            ctrl=3;
            digitalWrite(D7, LOW);
            x = rs485.read();//leitura para limpar o buffer
            delay(30);  
}                
           
void entrada_2() {
           digitalWrite(D7, HIGH);
           for (int i=0; i <=5 ;i++)
           {
           // Serial.println(le_entrada2[i]); //mostra no monitor serial o dado recebido
            rs485.write(le_entrada2[i]);
           }
           ctrl=4;
          digitalWrite(D7, LOW);
           delay(1000);  
           //x = rs485.read();
           }
void liga_led2() {
           digitalWrite(D7, HIGH);
           delay(10);
           for (int i=0; i <=5 ;i++)
           {
           // Serial.println(liga_2[i]); //mostra no monitor serial o dado recebido
            rs485.write(liga_2[i]);
           }
            ctrl=5;
            digitalWrite(D7, LOW);
            x = rs485.read(); //leitura para limpar o buffer
            delay(30);
           }
           
void desliga_led2() {
           digitalWrite(D7, HIGH);
           delay(10);
           for (int i=0; i <=5 ;i++)
           {
            // Serial.println(desliga_2[i]); //mostra no monitor se/rial o dado recebido
             rs485.write(desliga_2[i]);
           }
            ctrl=6;
            digitalWrite(D7, LOW);
            x = rs485.read(); //leitura para limpar o buffer
            delay(30);
}                
     
     
void setup() {
// Inicializa a comunicaÃ§Ã£o serial com uma taxa de 9600 bauds.
pinMode(D7,OUTPUT);
Serial.begin(9600);
rs485.begin(9600);
delay(50); 
}


void loop(){
if(Serial.available())
{
byte c = Serial.read(); // LÃª byte do buffer serial;
switch(c) {
case '\r': // Marca o fim de um comando.
case '\n':
if (size == 0) return;
buffer[size] = 0;
size = 0;
Serial.println("");
Serial.println(buffer);
//Serial.println("\n Dados transmitidos: ");
if (!strcmp(buffer,"le_entrada1")) entrada_1();
else if (!strcmp(buffer,"liga_led1")) liga_led1();
else if (!strcmp(buffer,"desliga_led1")) desliga_led1();
else if (!strcmp(buffer,"le_entrada2")) entrada_2();
else if (!strcmp(buffer,"liga_led2")) liga_led2();
else if (!strcmp(buffer,"desliga_led2")) desliga_led2();
else {
Serial.println("Comando invalido!");
Serial.println("Escreva proximo comando:");
return;            
}
//Serial.println("\n Dados recebidos ");
//Serial.println(rs485.available());
if(rs485.available()>0)
   {
      j = 0;
     while(j <=5)
      {
         z[j] = (rs485.read());
        // Serial.println(z[j]);
         j=j+1;    
      }
      checksum();
      if(checksum()==1 && ctrl==1)
      {
        Serial.print("\n\fPotenciometro escravo 1: ");
        Serial.print(z[4]);
        } 
       else if(checksum()==1 && ctrl==2)
       {
        Serial.println("\n\fLed 1 ligado!");
       }
        else if(checksum()==1 && ctrl==3)
       {
        Serial.println("\n\fLed 1 desligado!");
       }
        else if(checksum()==1 && ctrl==4)
       {
        Serial.print("\n\fTemperatura escravo 2: ");
        Serial.print(z[4]);
        Serial.print(" °C");

       }
        else if(checksum()==1 && ctrl==5)
       {
        Serial.println("\n\fLed 2 ligado!");
       }
        else if(checksum()==1 && ctrl==6)
       {
        Serial.println("\n\fLed 2 desligado!");
       }
        else{
        Serial.println("Erro de recebimento. Digite o comando novamente!");
        }
      }
digitalWrite(D7, LOW); 
Serial.println(" ");
Serial.println("\nEscreva proximo comando:");
break; 
// Adiciona carÃ¡cter ao buffer se nÃ£o estiver cheio.
default:
if (size < BUFFER_SIZE-1) {
buffer[size] = c;
++size;
}
break; 
}
}
}

