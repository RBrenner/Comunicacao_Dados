Fs = 100000000;            % Frequ�ncia de Amostragem                  
T = 1/Fs;                  % Per�odo de Amostragem     
L = 1500;                  % Tamanho do Sinal
t = (0:L-1)*T;             % Vetor de tempo para plot
x = 5*1000000;             % Frequ�ncia principal
S = 0;                     % Valor inicial da soma de sen�ides
Amp = 4;                   % Amplitude desejada do sinal original em Volts 
cont = 8;                  % N�mero de componentes(Principal + Harmonicas)
y = 1;                     %Contador inicial de ordem das componentes 

for i = 1:cont
    Soma = (4*Amp/(y*pi))*sin(2*pi*y*x*t);  % A cada ciclo soma harm�nicas �mpares
    S = S + Soma;                           % Atualiza��o do n�mero de componentes
    y = y + 2;
end
plot(1000000*t(1:80),S(1:80))
title('Sinal teste')
xlabel('t (microseconds)')
ylabel('X(t)')
%plot(t,S)